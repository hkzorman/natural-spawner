-- Entity registrations

local function ice_breaking_effect(pos)
	minetest.add_particlespawner({
			amount = math.random(15, 25),

			time = 0.5,
			-- Lifespan of spawner in seconds.
			-- If time is 0 spawner has infinite lifespan and spawns the `amount` on
			-- a per-second basis.
			minpos = {x=pos.x-1, y=pos.y+1, z=pos.z-1},
			maxpos = {x=pos.x+1, y=pos.y+1, z=pos.z+1},
			minvel = {x = -2.5, y = -2.5, z = -2.5},
			maxvel = {x = 2.5, y = 2.5, z = 2.5},


			-- minpos = {x=pos.x-1, y=pos.x-1, z=pos.x-1},
			-- maxpos = {x=pos.x+1, y=pos.x+1, z=pos.x+1},
			-- minvel = {x=0, y=0, z=0},
			-- maxvel = {x=0, y=0, z=0},
			minacc = {x=0, y=-10, z=0},
			maxacc = {x=0, y=-10, z=0},
			minexptime = 0.5,
			maxexptime = 1,
			minsize = 0.5,
			maxsize = 1,
			texture = "frozen_ice.png"
	})
end

local function normal_attack(self, to_attack)
	--set_animation(self, "punch")

--	local p2 = to_attack
--	local s2 = self.object:get_pos()

--	p2.y = p2.y + .5
--	s2.y = s2.y + .5

	--local line = minetest.line_of_sight(p2, s2)
	--minetest.log(dump(line))

--r, pos = minetest.line_of_sight(p2, s2, stepsize)
	--if line_of_sight(self, p2, s2) == true then

		-- play attack sound
		--mob_sound(self, self.sounds.attack)

		-- punch player (or what player is attached to)
		local attached = self.attack:get_attach()
		if attached then
			self.attack = attached
		end
		self.attack:punch(self.object, 1.0, {
			full_punch_interval = 1.0,
			damage_groups = {fleshy = self.damage}
		}, nil)
	--end

end

-- Default zombie
mobs:register_mob("natspawner:zombie", {
	type = "monster",
	passive = false,
	attack_type = "dogfight",
	damage = 4,
	reach = 2,
	hp_min = 12,
	hp_max = 35,
	armor = 150,
	collisionbox = {-0.25, -1, -0.3, 0.25, 0.75, 0.3},
	visual = "mesh",
	mesh = "creatures_mob.x",
	textures = {
		{"mobs_zombie.png"},
	},
	visual_size = {x=1, y=1},
	makes_footstep_sound = true,
	sounds = {
		random = "mobs_zombie.1",
		damage = "mobs_zombie_hit",
		attack = "mobs_zombie.3",
		death = "mobs_zombie_death",
	},
	walk_velocity = 0.5,
	run_velocity = 1.75,
	jump = true,
	floats = 0,
	view_range = 12,
	drops = {
		{name = "zombie:rotten_flesh",
		chance = 2, min = 3, max = 5,},
	},
	water_damage = 0,
	lava_damage = 1,
	light_damage = 0,
	animation = {
		speed_normal = 10,		speed_run = 15,
		stand_start = 0,		stand_end = 79,
		walk_start = 168,		walk_end = 188,
		run_start = 168,		run_end = 188,
--		punch_start = 168,		punch_end = 188,
	},
	on_rightclick = function(self, clicker)
		minetest.log(dump(self))
	end,
	on_die = natspawner.on_mob_die
})

mobs:register_mob("natspawner:giant_zombie", {
	type = "monster",
	passive = false,
	attack_type = "dogfight",
	damage = 4,
	reach = 2,
	hp_min = 12,
	hp_max = 35,
	armor = 150,
	collisionbox = {-0.625, -2.5, -2.8, 0.625, 1.875, 2.8},
	visual = "mesh",
	mesh = "creatures_mob.x",
	textures = {
		{"mobs_zombie.png"},
	},
	visual_size = {x=2.5, y=2.5, z=2.5},
	makes_footstep_sound = true,
	sounds = {
		random = "mobs_zombie.1",
		damage = "mobs_zombie_hit",
		attack = "mobs_zombie.3",
		death = "mobs_zombie_death",
	},
	walk_velocity = 0.5,
	run_velocity = 1.75,
	jump = true,
	floats = 0,
	view_range = 12,
	drops = {
		{name = "zombie:rotten_flesh",
		chance = 2, min = 3, max = 5,},
	},
	water_damage = 0,
	lava_damage = 1,
	light_damage = 0,
	animation = {
		speed_normal = 10,		speed_run = 15,
		stand_start = 0,		stand_end = 79,
		walk_start = 168,		walk_end = 188,
		run_start = 168,		run_end = 188,
--		punch_start = 168,		punch_end = 188,
	},
	on_rightclick = function(self, clicker)
		minetest.log(dump(self))
	end,
	on_die = natspawner.on_mob_die
})

mobs:register_mob("natspawner:ice_zombie", {
	type = "monster",
	passive = false,
	attack_type = "dogfight",
	damage = 4,
	reach = 2,
	hp_min = 12,
	hp_max = 35,
	armor = 150,
	collisionbox = {-0.25, -1, -0.3, 0.25, 0.75, 0.3},
	visual = "mesh",
	mesh = "creatures_mob.x",
	textures = {
		{"mobs_zombie_ice.png"},
	},
	visual_size = {x=1, y=1},
	makes_footstep_sound = true,
	sounds = {
		random = "mobs_zombie",
		damage = "mobs_zombie_ice_hit",
		attack = "mobs_zombie",
		death = "mobs_zombie_death",
	},
	walk_velocity = 0.5,
	run_velocity = 1.75,
	jump = true,
	floats = 0,
	view_range = 12,
	drops = {
		{name = "zombie:rotten_flesh",
		chance = 2, min = 3, max = 5,},
	},
	water_damage = 0,
	lava_damage = 1,
	light_damage = 0,
	animation = {
		speed_normal = 10,		speed_run = 15,
		stand_start = 0,		stand_end = 79,
		walk_start = 168,		walk_end = 188,
		run_start = 168,		run_end = 188,
--		punch_start = 168,		punch_end = 188,
	},
	blood_amount = 4,
	blood_texture = "default_ice.png",
	after_activate = function(self)
		self.ice_attack_timer = 0
	end,
	on_rightclick = function(self, clicker)
		minetest.log(dump(self))
	end,
	on_die = natspawner.on_mob_die,
	custom_attack = function(self, to_attack)

		self.ice_attack_timer = self.ice_attack_timer + 1

		if self.ice_attack_timer >= 3 then
			self.ice_attack_timer = 0
			local chance = math.random(1, 5)
			if chance > 3 then
				-- TODO: Add particle effects
				minetest.log("Ice")
				minetest.set_node(to_attack, {name="natspawner:freezing_ice"})
				minetest.set_node({x=to_attack.x, y=to_attack.y+1, z=to_attack.z}, {name="natspawner:freezing_ice"})
				-- Remove node
				minetest.after(math.random(1,2), function(pos)
					local pos_above = {x=pos.x, y=pos.y+1, z=pos.z}
					minetest.sound_play("default_break_glass")
					ice_breaking_effect(pos)
					ice_breaking_effect(pos_above)
					minetest.set_node(pos, {name="air"})
					minetest.set_node(pos_above, {name="air"})
				end, to_attack)
			end
		end

		normal_attack(self, to_attack)
	end
})

mobs:register_mob("natspawner:sand_zombie_small", {
	type = "monster",
	passive = false,
	attack_type = "dogfight",
	damage = 6,
	reach = 2,
	hp_min = 12,
	hp_max = 35,
	armor = 100,
	collisionbox = {-0.1625, -0.65, -0.195, 0.1625, 0.4875, 0.195},
	visual = "mesh",
	mesh = "creatures_mob.x",
	textures = {
		{"mobs_zombie_sand.png"},
	},
	visual_size = {x=0.675, y=0.65, z=0.675},
	makes_footstep_sound = true,
	sounds = {
		random = "mobs_zombie",
		damage = "mobs_zombie_sand_hit",
		attack = "mobs_zombie",
		death = "mobs_zombie_death",
	},
	walk_velocity = 1.5,
	run_velocity = 3,
	jump = true,
	floats = 0,
	view_range = 12,
	drops = {
		{name = "zombie:rotten_flesh",
		chance = 2, min = 3, max = 5,},
	},
	water_damage = 0,
	lava_damage = 1,
	light_damage = 0,
	animation = {
		speed_normal = 10,		speed_run = 15,
		stand_start = 0,		stand_end = 79,
		walk_start = 168,		walk_end = 188,
		run_start = 168,		run_end = 188,
--		punch_start = 168,		punch_end = 188,
	},
	blood_amount = 7,
	blood_texture = "default_sand.png",
	on_rightclick = function(self, clicker)
		minetest.log(dump(self))
	end,
	on_die = natspawner.on_mob_die
})

mobs:register_mob("natspawner:sand_zombie", {
	type = "monster",
	passive = false,
	attack_type = "dogfight",
	damage = 4,
	reach = 2,
	hp_min = 12,
	hp_max = 35,
	armor = 150,
	collisionbox = {-0.25, -1, -0.3, 0.25, 0.75, 0.3},
	visual = "mesh",
	mesh = "creatures_mob.x",
	textures = {
		{"mobs_zombie_sand.png"},
	},
	visual_size = {x=1, y=1},
	makes_footstep_sound = true,
	sounds = {
		random = "mobs_zombie",
		damage = "mobs_zombie_sand_hit",--"mobs_zombie_hit",
		attack = "mobs_zombie",
		death = "mobs_zombie_death",
	},
	walk_velocity = 0.5,
	run_velocity = 1.75,
	jump = true,
	floats = 0,
	view_range = 12,
	drops = {
		{name = "zombie:rotten_flesh", chance = 2, min = 3, max = 5},
	},
	water_damage = 0,
	lava_damage = 1,
	light_damage = 0,
	animation = {
		speed_normal = 10,		speed_run = 15,
		stand_start = 0,		stand_end = 79,
		walk_start = 168,		walk_end = 188,
		run_start = 168,		run_end = 188,
--		punch_start = 168,		punch_end = 188,
	},
	blood_amount = 7,
	blood_texture = "default_sand.png",
	on_rightclick = function(self, clicker)
		minetest.log(dump(self))
	end,
	on_die = function(self, pos)

		-- TODO: Add particle effects
		--minetest.add_particle_spawner()
		local positions = {}
		for x = -1, 1 do
		for z = -1, 1 do
			local chance = math.random(1, 2)
			if (chance > 1) then
				minetest.set_node({x=pos.x + x, y=pos.y-1, z=pos.z + z}, {name="natspawner:evil_sand"})
				positions[#positions + 1] = {x=pos.x + x, y = pos.y-1, z=pos.z + z}
			end
		end
		end

		minetest.after(1, function(positions)
			for i = 1, #positions do
				local chance = math.random(1, 10)
				if (chance > 5) then
					minetest.add_entity({x=positions[i].x, y=positions[i].y+1, z=positions[i].z}, "natspawner:sand_zombie_small")
				end
			end
			--minetest.add_entity({x=pos.x - 1, y=pos.y, z=pos.z}, "natspawner:sand_zombie_small")
		end, positions)

		-- Spawner callback
		natspawner.on_mob_die(self, pos)
	end
})

-- Nodes



minetest.register_node("natspawner:freezing_ice", {
	description = "Freezing Ice",
	tiles = {"frozen_ice.png"},
	groups = {cracky = 3, cools_lava = 1, slippery = 3, flammable = 2, liquid = 1},
	use_texture_alpha = true,
	sunlight_propagates = true,
	drawtype = "glasslike",
	paramtype = "light",
	sounds = default.node_sound_glass_defaults(),
	walkable = false,
	pointable = true,
	diggable = true,
	buildable_to = false,
	drop = "",
	drowning = 1,
	liquid_renewable = false,
	liquidtype = "source",
	liquid_range= 0,
	liquid_alternative_flowing = "natspawner:freezing_ice",
	liquid_alternative_source = "natspawner:freezing_ice",
	liquid_viscosity = 30,
	post_effect_color = {a = 128, r = 30, g = 60, b = 90},
	--groups = {cracky = 3, cools_lava = 1, slippery = 3, flammable=2, liquid=1, not_in_creative_inventory=1},
})

minetest.register_node("natspawner:evil_sand", {
	description = "Evil Sand",
	tiles = {"default_sand.png"},
	inventory_image = "default_snowball.png",
	wield_image = "default_snowball.png",
	paramtype = "light",
	buildable_to = true,
	floodable = true,
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, -0.5, 0.5, -0.25, 0.5},
		},
	},
	collision_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, -0.5, 0.5, -7 / 16, 0.5},
		},
	},
	groups = {crumbly = 3, falling_node = 1, sand = 1},
	sounds = default.node_sound_sand_defaults(),
	damage_per_second = 2
})
